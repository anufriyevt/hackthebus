from django.urls import path
from rest_framework import routers

from .views import CustomObtainAuthToken, UserViewSet


router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
urlpatterns = router.urls

urlpatterns += [
    path('auth/', CustomObtainAuthToken.as_view())
]
