from rest_framework import parsers, renderers, viewsets, status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from user.models import User
from user.serializers import UserSerializer, CustomAuthTokenSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
        retrieve:
        Return the user specified by id.

        list:
        Return a list of all the existing users.

        create:
        Create a new user.

        delete:
        Delete the user specified by id.

    """
    queryset = User.objects.all()
    serializer_user = UserSerializer

    def get_permissions(self):
        if self.action in ['create', 'list', 'retrieve']:
            self.permission_classes = ()

        if self.action in ['destroy', 'set_rating']:
            self.permission_classes = (IsAuthenticated,)

        return super(UserViewSet, self).get_permissions()

    def get_serializer_class(self):
        return self.serializer_user

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid():
            data = self.request.data
            user = User.objects.create_user(name=data['name'], phone=data['phone'],
                                            password=data['password'])
            headers = self.get_success_headers(data)
            token = Token.objects.get(user_id=user.pk)

            return Response({'id': token.user_id, 'phone': user.phone,
                             'name': user.name, 'rating': user.rating,
                             'token': token.key},
                            status=status.HTTP_201_CREATED, headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], url_path='rating')
    def set_rating(self, request, pk=None):
        data = self.request.data
        user = User.objects.get(id=pk)

        if user is not None:
            try:
                rate = int(data['rating'])
                if 1 <= rate <= 5:
                    user.rates_sum += rate
                else:
                    raise ValueError
                user.rates_number += 1
            except ValueError:
                return Response(data={'rating': ['Rating should be integer number from 1 to 5']},
                                status=status.HTTP_400_BAD_REQUEST)

            user.save()
            return Response({'id': user.id, 'phone': user.phone,
                             'name': user.name, 'rating': user.rating},
                            status=status.HTTP_200_OK)
        return Response(self.serializer_user.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = CustomAuthTokenSerializer

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    def get_serializer(self, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request, *_args, **_kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, _created = Token.objects.get_or_create(user=user)
        return Response({'id': token.user_id, 'name': user.name,
                         'phone': user.phone, 'rating': user.rating,
                         'token': token.key})
