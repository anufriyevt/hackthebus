from django.core.validators import RegexValidator
from django.test import TestCase
from rest_framework.authtoken.models import Token

from user.models import User


class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(phone='+98887773344', name='Alice', password='admin')

    def test_phone_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('phone')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'phone')

        field_length = field.max_length
        self.assertEqual(field_length, 17)

        field_regex = field.validators[0]
        self.assertEqual(field_regex,
                         RegexValidator(regex=r'^\+\d{8,15}$',
                                        message="Phone number must be entered in the format: "
                                                "'+999999999'. Up to 15 digits allowed."))

        field_unique = field.unique
        self.assertEqual(field_unique, True)

        field_error_messages = field.error_messages
        self.assertEqual(field_error_messages.get('unique'),
                         "A user with that phone already exists.")

    def test_name_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('name')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'name')

        field_length = field.max_length
        self.assertEqual(field_length, 150)

        field_blank = field.blank
        self.assertEqual(field_blank, False)

    def test_email_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('email')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'email')

        field_length = field.max_length
        self.assertEqual(field_length, 150)

        field_blank = field.blank
        self.assertEqual(field_blank, True)

    def test_rates_number_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('rates_number')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'rates number')

        field_default = field.default
        self.assertEqual(field_default, 0)

    def test_rates_sum_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('rates_sum')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'rates sum')

        field_default = field.default
        self.assertEqual(field_default, 0)

    def test_is_stuff_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('is_staff')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'staff status')

        field_default = field.default
        self.assertEqual(field_default, False)

        field_help_text = field.help_text
        self.assertEqual(field_help_text,
                         'Designates whether the user can log into this admin site.')

    def test_is_active_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('is_active')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'active')

        field_default = field.default
        self.assertEqual(field_default, True)

        field_help_text = field.help_text
        self.assertEqual(field_help_text,
                         'Designates whether this user should be treated as active. '
                         'Unselect this instead of deleting accounts.')

    def test_date_joined_field(self):
        user = User.objects.get(id=1)
        field = user._meta.get_field('date_joined')

        field_label = field.verbose_name
        self.assertEqual(field_label, 'date joined')

    def test_get_object_name(self):
        user = User.objects.get(id=1)

        expected_object_name = f'{user.name}'
        self.assertEqual(expected_object_name, str(user))

    def test_create_auth_token(self):
        user = User.objects.get(id=1)
        token = Token.objects.filter(user=user)

        self.assertIsNotNone(token)


class UserManagerTestCase(TestCase):

    def test_main_create_user(self):
        user = User.objects.create_user(phone='+98887773344', name='Alice', password='admin')

        self.assertTrue(isinstance(user, User))
        self.assertEqual(user.is_staff, False)
        self.assertEqual(user.is_superuser, False)

    def test_main_create_superuser(self):
        user = User.objects.create_superuser(phone='+98887773344', name='Alice', password='admin')

        self.assertTrue(isinstance(user, User))
        self.assertEqual(user.is_staff, True)
        self.assertEqual(user.is_superuser, True)
