from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from posts.models import User


class APIUserTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(phone='+98887773344', name='Mary', password='admin')
        User.objects.create(phone='+45777787645', name='Bob', password='admin')

    def test_create_valid(self):
        response = self.client.post(path='/users/', data={'name': 'Alice',
                                                          'phone': '+9999999999',
                                                          'password': 'admin'})
        token = Token.objects.get(user_id=3)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), {'id': 3, 'phone': '+9999999999',
                                           'name': 'Alice', 'rating': 0,
                                           'token': token.key})

    def test_create_invalid_phone(self):
        response = self.client.post(path='/users/', data={'name': 'Alice',
                                                          'phone': '+9888',
                                                          'password': 'admin'})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            "phone": [
                "Phone number must be entered in the format: "
                "'+999999999'. Up to 15 digits allowed."
            ]
        })

    def test_create_duplicate_user(self):
        response = self.client.post(path='/users/', data={'name': 'Alice',
                                                          'phone': '+98887773344',
                                                          'password': 'admin'})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            "phone": [
                'A user with that phone already exists.'
            ]
        })

    def test_list_users(self):
        response = self.client.get(path='/users/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [{'id': 1, 'phone': '+98887773344',
                                            'rating': 0, 'name': 'Mary'},
                                           {'id': 2, 'phone': '+45777787645',
                                            'rating': 0, 'name': 'Bob'}])

    def test_retrieve_users(self):
        response = self.client.get(path='/users/1/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'id': 1, 'phone': '+98887773344',
                                           'rating': 0, 'name': 'Mary'})

    def test_delete_user(self):
        token = Token.objects.get(user_id=1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.delete(path='/users/1/')

        self.assertEqual(response.status_code, 204)

    def test_set_rating_valid(self):
        token = Token.objects.get(user_id=2)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.post(path='/users/2/rating/', data={'rating': 5})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'id': 2, 'phone': '+45777787645',
                                           'name': 'Bob', 'rating': 5.0})

    def test_set_rating_invalid(self):
        token = Token.objects.get(user_id=2)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.post(path='/users/2/rating/', data={'rating': 20.8})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            "rating": [
                "Rating should be integer number from 1 to 5"
            ]
        })


class APIAuthTokenTest(TestCase):

    def test_auth_valid(self):
        self.client.post(path='/users/', data={'name': 'Alice',
                                               'phone': '+9999999999',
                                               'password': 'admin'})

        response = self.client.post(path='/auth/', data={'phone': '+9999999999',
                                                         'password': 'admin'})
        token = Token.objects.get(user_id=1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'id': 1, 'phone': '+9999999999',
                                           'name': 'Alice', 'rating': 0,
                                           'token': token.key})

    def test_auth_invalid(self):
        response = self.client.post(path='/auth/', data={'phone': '+345',
                                                         'password': 'admin'})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            "non_field_errors": [
                "Unable to log in with provided credentials."
            ]
        })
