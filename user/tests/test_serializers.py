from django.test import TestCase
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from user.models import User
from user.serializers import UserSerializer, CustomAuthTokenSerializer


class CustomUserSerializerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(phone='+98887773344', name='Alice', password='admin')

    def test_contains_expected_fields(self):
        user = User.objects.get(id=1)
        serializer = UserSerializer(instance=user)
        data = serializer.data

        self.assertEqual(set(data.keys()), {'id', 'phone', 'rating', 'name'})

    def test_field_content(self):
        user = User.objects.get(id=1)
        serializer = UserSerializer(instance=user)
        data = serializer.data

        self.assertEqual(data, {
            'id': 1,
            'phone': '+98887773344',
            'rating': 0,
            'name': 'Alice'
        })

    def test_create(self):
        user_attributes = {
            'phone': '+98887773347',
            'password': 'admin',
            'name': 'Alice'
        }
        serializer = UserSerializer()
        user = serializer.create(user_attributes)

        self.assertTrue(isinstance(user, User))
        self.assertEqual(user.phone, '+98887773347')
        self.assertEqual(user.name, 'Alice')


class CustomAuthTokenSerializerTest(TestCase):

    def test_contains_expected_fields(self):
        self.client.post(path='/users/', data={'name': 'Alice',
                                               'phone': '+9999999999',
                                               'password': 'admin'})
        token = Token.objects.get(user_id=1)
        serializer = CustomAuthTokenSerializer(instance=token)
        data = serializer.data

        self.assertEqual(set(data.keys()), set())

    def test_validate_valid(self):
        self.client.post(path='/users/', data={'name': 'Alice',
                                               'phone': '+9999999995',
                                               'password': 'admin'})
        user_attributes = {
            'phone': '+9999999995',
            'password': 'admin'
        }
        serializer = CustomAuthTokenSerializer()
        attrs = serializer.validate(user_attributes)

        self.assertEqual(attrs, user_attributes)

    def test_validate_invalid_1(self):
        user_attributes = {
            'phone': '+99999934',
            'password': 'admin'
        }
        serializer = CustomAuthTokenSerializer()

        with self.assertRaisesRegex(serializers.ValidationError,
                                    'Unable to log in with provided credentials.'):
            serializer.validate(user_attributes)

    def test_validate_invalid_2(self):
        user_attributes = {
            'phone': '+9999999995'
        }
        serializer = CustomAuthTokenSerializer()

        with self.assertRaisesRegex(serializers.ValidationError,
                                    'Must include "phone" and "password".'):
            serializer.validate(user_attributes)
