FROM python:3.7-alpine
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
MAINTAINER My Project

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

WORKDIR /hackthebus
COPY ./ /hackthebus

RUN adduser -D user
RUN chown user:user -R /hackthebus
USER user

CMD python manage.py runserver 0.0.0.0:$PORT
