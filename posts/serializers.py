from rest_framework import serializers

from .models import Post, Travel


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            "id",
            "route_from",
            "route_to",
            "departure_date",
            "pub_date",
            "author",
            "type",
        )
        extra_kwargs = {
            'author': {'read_only': True},
            'type': {'read_only': True},
        }


class ChoosePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Travel
        fields = (
            'post',
            'user',
        )
        extra_kwargs = {
            'user': {'read_only': True},
            'post': {'read_only': True},
        }

    def update(self, instance: Post, validated_data):
        Travel.objects.get_or_create(post=instance, user=self.context['request'].user)
        return instance


class RideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Travel
        fields = ("post", "user")


class PhoneSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(source='author.phone', read_only=True)

    class Meta:
        model = Post
        fields = ('phone_number',)
