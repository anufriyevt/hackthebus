from datetime import datetime, timedelta

from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from posts.models import Post, Travel
from posts.serializers import PostSerializer, ChoosePostSerializer, RideSerializer, PhoneSerializer
from posts.permissions import IsOwner


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.filter(
        departure_date__gte=datetime.now() - timedelta(minutes=30)
    ).filter(
        type="Driver"
    )
    serializer_class = PostSerializer

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = (IsAuthenticated,)
        if self.request.method == 'PATCH':
            self.permission_classes = (IsAuthenticated, IsOwner)
        if self.request.method == 'DELETE':
            self.permission_classes = (IsAuthenticated, IsOwner)
        return super(PostViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(author=request.user, type="Driver")  # pass current user
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    @action(detail=False, methods=['GET'])
    def closest_ride(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        closest_greater_qs = queryset.filter(
            departure_date__gte=datetime.now()
        ).filter(
            type="Driver"
        ).order_by('departure_date')[:1]

        closest_less_qs = queryset.filter(
            departure_date__lte=datetime.now()
        ).filter(
            type="Driver"
        ).order_by('-departure_date')[:1]
        if len(closest_greater_qs) == 0:
            queryset = closest_less_qs

        elif len(closest_less_qs) == 0:
            queryset = closest_greater_qs
        else:
            first = closest_greater_qs[0]
            second = closest_less_qs[0]
            if first.departure_date - timezone.now() < abs(second.departure_date - timezone.now()):
                queryset = closest_greater_qs
            else:
                queryset = closest_less_qs

        serializer = PostSerializer(instance=queryset, many=True)
        return Response(serializer.data)

    @permission_classes([IsAuthenticated])
    @action(detail=True, methods=["POST"])
    def choose(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = ChoosePostSerializer(
            data=request.data,
            context={
                'request': request
            },
            instance=instance,
            partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response()

    @permission_classes([IsAuthenticated])
    @action(detail=False, methods=["GET"])
    def my_travels(self, request, *args, **kwargs):
        current_user = request.user
        queryset = Travel.objects.filter(user=current_user)
        serializer = RideSerializer(instance=queryset, many=True)
        return Response(serializer.data)

    @permission_classes([IsAuthenticated])
    @action(detail=False, methods=["GET"])
    def my_rides(self, request, *args, **kwargs):
        current_user = request.user
        queryset = Post.objects.filter(author=current_user, type="Driver")
        serializer = PostSerializer(instance=queryset, many=True)
        return Response(serializer.data)

    @permission_classes([IsAuthenticated])
    @action(detail=True, methods=["GET"])
    def show_phone(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = PhoneSerializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)


class PassengerViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.filter(
        departure_date__gte=datetime.now() - timedelta(minutes=30)
    ).filter(
        type="Passenger"
    )
    serializer_class = PostSerializer

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = (IsAuthenticated,)
        if self.request.method == 'PATCH':
            self.permission_classes = (IsAuthenticated, IsOwner)
        if self.request.method == 'DELETE':
            self.permission_classes = (IsAuthenticated, IsOwner)
        return super(PassengerViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(author=request.user, type="Passenger")  # pass current user
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @permission_classes([IsAuthenticated])
    @action(detail=True, methods=["GET"])
    def show_phone(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = PhoneSerializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)
