from rest_framework import routers

from posts.views import PostViewSet, PassengerViewSet

router = routers.SimpleRouter()
router.register(r"posts", PostViewSet)
router.register(r"passengers", PassengerViewSet)

urlpatterns = []
urlpatterns += router.urls
