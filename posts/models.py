from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Post(models.Model):
    route_from = models.CharField('From', max_length=200)
    route_to = models.CharField('To', max_length=200)
    departure_date = models.DateTimeField('Departure time')
    pub_date = models.DateTimeField('Date published', auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    TYPE_CHOICE = (
        ("DRIVER", "Driver"),
        ("PASSENGER", "Passenger"),
    )
    type = models.CharField(choices=TYPE_CHOICE, default="DRIVER", max_length=10)

    def __str__(self):
        return f'{self.type}  From {self.route_from}  To {self.route_to}'

    class Meta:
        ordering = ["-departure_date"]


class Travel(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    class Meta:
        unique_together = ("user", "post")
