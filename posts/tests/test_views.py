from django.test import TestCase
from django.utils import timezone
from rest_framework.authtoken.models import Token

from posts.models import User, Post


class APIPostTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(phone='+98887773344', name='Mary', password='admin')
        cls.id = user.id
        post = Post.objects.create(
            route_from="Innop",
            route_to="Kazan",
            departure_date=timezone.now(),
            author=user,
            type="Driver",
        )
        cls.post_id = post.id
        passenger = Post.objects.create(
            route_from="Innop",
            route_to="Kazan",
            departure_date=timezone.now(),
            author=user,
            type="Passenger",
        )
        cls.passenger_id = passenger.id

    def test_create_valid(self):
        token = Token.objects.get(user_id=APIPostTest.post_id)
        response = self.client.post(
            path='/posts/',
            data={
                "route_from": "Innop",
                "route_to": "Kazan",
                "departure_date": "2021-03-08 00:50"
            },
            HTTP_AUTHORIZATION='Token {}'.format(token)
        )
        self.assertEqual(response.status_code, 201)
        pub_date = response.json()["pub_date"]
        self.assertEqual(response.json(), {
            "id": response.json()["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": "2021-03-08T00:50:00+03:00",
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Driver"
        })

    def test_create_unauthorized(self):
        response = self.client.post(
            path='/posts/',
            data={
                "route_from": "Innop",
                "route_to": "Kazan",
                "departure_date": "2021-03-08 00:50"
            }
        )
        self.assertEqual(response.status_code, 401)

    def test_list_posts(self):
        response = self.client.get(path='/posts/')

        self.assertEqual(response.status_code, 200)
        pub_date = response.json()[0]["pub_date"]
        dep_time = response.json()[0]["departure_date"]
        self.assertEqual(response.json(), [{
            "id": response.json()[0]["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": dep_time,
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Driver"
        }])

    def test_retrieve_posts(self):
        response = self.client.get(path=f'/posts/{APIPostTest.post_id}/')
        self.assertEqual(response.status_code, 200)

        pub_date = response.json()["pub_date"]
        dep_time = response.json()["departure_date"]
        self.assertEqual(response.json(), {
            "id": response.json()["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": dep_time,
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Driver"
        })

    def test_delete_post(self):
        response = self.client.delete(path=f'/posts/{APIPostTest.post_id}/')

        self.assertEqual(response.status_code, 401)

    def test_show_phone(self):
        response = self.client.get(path=f'/posts/{APIPostTest.post_id}/show_phone/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"phone_number": "+98887773344"})

    def test_create_passenger_valid(self):
        token = Token.objects.get(user_id=APIPostTest.id)
        response = self.client.post(
            path='/passengers/',
            data={
                "route_from": "Innop",
                "route_to": "Kazan",
                "departure_date": "2021-03-08 00:50"
            },
            HTTP_AUTHORIZATION='Token {}'.format(token)
        )
        self.assertEqual(response.status_code, 201)
        pub_date = response.json()["pub_date"]
        self.assertEqual(response.json(), {
            "id": response.json()["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": "2021-03-08T00:50:00+03:00",
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Passenger"
        })

    def test_create_passenger_unauthorized(self):
        response = self.client.post(
            path='/passengers/',
            data={
                "route_from": "Innop",
                "route_to": "Kazan",
                "departure_date": "2021-03-08 00:50"
            }
        )
        self.assertEqual(response.status_code, 401)

    def test_list_passengers(self):
        response = self.client.get(path='/passengers/')

        self.assertEqual(response.status_code, 200)
        pub_date = response.json()[0]["pub_date"]
        dep_time = response.json()[0]["departure_date"]
        self.assertEqual(response.json(), [{
            "id": response.json()[0]["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": dep_time,
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Passenger"
        }])

    def test_retrieve_passenger(self):
        response = self.client.get(path=f'/passengers/{APIPostTest.passenger_id}/')
        self.assertEqual(response.status_code, 200)

        pub_date = response.json()["pub_date"]
        dep_time = response.json()["departure_date"]
        self.assertEqual(response.json(), {
            "id": response.json()["id"],
            "route_from": "Innop",
            "route_to": "Kazan",
            "departure_date": dep_time,
            "pub_date": pub_date,
            "author": APIPostTest.id,
            "type": "Passenger"
        })

    def test_delete_passenger(self):
        response = self.client.delete(path=f'/passengers/{APIPostTest.passenger_id}/')

        self.assertEqual(response.status_code, 401)

    def test_show_passenger_phone(self):
        response = self.client.get(path=f'/passengers/{APIPostTest.passenger_id}/show_phone/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"phone_number": "+98887773344"})
