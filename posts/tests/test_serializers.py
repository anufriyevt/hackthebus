from django.test import TestCase
from django.utils import timezone

from posts.models import Post
from posts.serializers import PostSerializer
from user.models import User


class PostSerializerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(phone='+98887773344', name='Alice', password='admin')
        post = Post.objects.create(
            route_from="Innop",
            route_to="Kazan",
            departure_date=timezone.now(),
            author=user,
            type="Driver",
        )
        cls.post_id = post.id
        cls.user_id = user.id

    def test_contains_expected_content(self):
        post = Post.objects.get(id=PostSerializerTest.post_id)
        serializer = PostSerializer(instance=post)
        data = serializer.data

        self.assertEqual(set(data.keys()), {
            "id",
            "route_from",
            "route_to",
            "departure_date",
            "pub_date",
            "author",
            "type",
        })

    def test_field_content(self):
        post = Post.objects.get(id=PostSerializerTest.post_id)
        serializer = PostSerializer(instance=post)
        data = serializer.data
        self.assertEqual(data["route_from"], "Innop")
        self.assertEqual(data["route_to"], "Kazan")
        self.assertEqual(data["author"], PostSerializerTest.user_id)
        self.assertEqual(data["type"], "Driver")
