import datetime as dt

from django.test import TestCase
from django.utils import timezone

from posts.models import Post, Travel
from user.models import User


class PostModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create(
            phone='+79770002211',
            email='someemail@gmail.com'
        )
        cls.date_now = timezone.now()
        cls.post = Post.objects.create(
            route_from='Innopolis',
            route_to='Kazan',
            departure_date='2021-03-04 23:59',
            author=cls.user,
            type='Driver',
        )

    def test_post_object_name(self):
        post = PostModelTest.post
        expected_object_name = (f'{post.type}  From {post.route_from}'
                                f'  To {post.route_to}')
        self.assertEqual(expected_object_name, str(post))

    def test_post_route_from_field(self):
        post = PostModelTest.post
        self.assertEqual('Innopolis', post.route_from)

        field = post._meta.get_field('route_from')
        field_length = field.max_length
        self.assertEqual(field_length, 200)

    def test_post_route_to_field(self):
        post = PostModelTest.post
        self.assertEqual('Kazan', post.route_to)

        field = post._meta.get_field('route_to')
        field_length = field.max_length
        self.assertEqual(field_length, 200)

    def test_post_departure_date_field(self):
        post = PostModelTest.post
        self.assertEqual('2021-03-04 23:59', post.departure_date)

        field = post._meta.get_field('departure_date')
        field_label = field.verbose_name
        self.assertEqual(field_label, 'Departure time')

    def test_post_author_phone_field(self):
        post = PostModelTest.post
        self.assertEqual('+79770002211', post.author.phone)

    def test_post_pub_date_field(self):
        post = PostModelTest.post
        self.assertAlmostEqual(
            self.date_now, post.pub_date,
            delta=dt.timedelta(seconds=2)
        )

        field = post._meta.get_field('pub_date')
        field_label = field.verbose_name
        self.assertEqual(field_label, 'Date published')

    def test_post_type_field(self):
        post = PostModelTest.post
        self.assertEqual('Driver', post.type)

        field = post._meta.get_field('type')
        field_length = field.max_length
        self.assertEqual(field_length, 10)


class TravelModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create(
            phone='+79770002211',
            email='someemail@gmail.com'
        )
        cls.date_now = timezone.now()
        cls.post = Post.objects.create(
            route_from='Innopolis',
            route_to='Kazan',
            departure_date='2021-03-04 23:59',
            author=cls.user,
            type='Driver',
        )
        cls.travel = Travel.objects.create(
            post=cls.post,
            user=cls.user,
        )

    def test_travel_is_filled(self):
        travel = TravelModelTest.travel
        self.assertIsInstance(travel.post, Post)
        self.assertIsInstance(travel.user, User)

    def test_travel_correct_user(self):
        travel = TravelModelTest.travel
        self.assertEqual('+79770002211', travel.user.phone)

    def test_travel_correct_post(self):
        travel = TravelModelTest.travel
        expected_object_name = (f'{travel.post.type}  From '
                                f'{travel.post.route_from}  '
                                f'To {travel.post.route_to}')
        self.assertEqual(expected_object_name, str(travel.post))
